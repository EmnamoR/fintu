import os
import unittest

import pandas as pd

from app.configs.config import get_config


class DFCorpusTests(unittest.TestCase):
  """ class for running unittests """

  def setUp(self):
    """ Your setUp """
    config = get_config()
    try:
      data = pd.read_json(os.path.join(os.getcwd() + config.corpus_input))
    except IOError:
      print('cannot open file')
    self.fixture = data
