FROM python:3.6

RUN pip install --upgrade setuptools
ENV PYTHONPATH "${PYTHONPATH}:/usr/bin/python3.6"

COPY requirements.txt /
RUN pip install -r /requirements.txt --upgrade --force-reinstall
COPY . /fintu
WORKDIR /fintu


CMD python app/main.py
