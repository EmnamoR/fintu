## Instructions
### Build docker image 
``docker build -t fintu .``
### Run Docker image
We need to mount a volume to send data from our local machine to the container and save the outputfile outside of the container (in the mounted volume). We could also build the image with data inside it, but then we want be able to change the file inputs if wanted. </br>
```docker run -v path_to_files_dir:/data/  -t fintu:latest python app/main.py  --customers_input /data/customers.json --corpus_input /data/corpus.json --output_file /data/out.csv ```
#### Ex:
```docker run -v /Users/emnamor/Desktop/task/:/data/  -t fintu:latest python app/main.py  --customers_input /data/customers.json --corpus_input /data/corpus.json --output_file /data/out.csv ```