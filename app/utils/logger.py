import logging
import sys


def get_logger(logger_name):
  logger = logging.getLogger(logger_name)
  logger.setLevel(logging.DEBUG)
  ch = logging.StreamHandler(sys.stdout)
  formatter = logging.Formatter('[%(filename)s][%(funcName)s:%(lineno)d]' + \
                                '[%(levelname)s] %(message)s'
                                )
  ch.setFormatter(formatter)
  ch.setLevel(logging.DEBUG)
  logger.addHandler(ch)
  logger.propagate = False

  return logger
