class Error(Exception):
  # Error is derived class for Exception, but
  # Base class for exceptions in this module
  pass


class MissingConfigArgsErrorMeta(Error):
  """Raised ars are missed"""
  pass

  def __str__(self):
    return ' Please run main.py [-h] [--mode MODE] [--customers_input ' \
           'CUSTOMERS_INPUT] [--corpus_input CORPUS_INPUT] [--output_file ' \
           'OUTPUT_FILE]'


class FileNotFoundErrorMeta(Error):
  """Raised args are missed"""
  pass

  def __str__(self):
    return 'Please check File Path'
