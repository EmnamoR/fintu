import os

import pandas as pd

from app.utils.exceptions import FileNotFoundErrorMeta
from app.utils.logger import get_logger

logger = get_logger(__name__)

"""

"""

class Analyser(object):
  def __init__(self, config):
    self.config = config
    # check file exists
    customer_filepath = self.config.customers_input
    corpus_filepath = self.config.corpus_input
    if not os.path.isfile(customer_filepath):
      logger.error('customers filepath  {} not found'.format(customer_filepath))
      raise FileNotFoundErrorMeta
    if not os.path.isfile(corpus_filepath):
      logger.error('corpus filepath  {} not found'.format(customer_filepath))
      raise FileNotFoundErrorMeta
    self.customers = pd.read_json(customer_filepath)
    self.corpus = pd.read_json(corpus_filepath, orient='records')


  def _clean_corpus_data(self):
    corpus_exploded = self.corpus.explode('categories').reset_index(drop=True)
    categories = pd.DataFrame(
      corpus_exploded['categories'].values.tolist()).rename(
      columns={'name': 'category_name'})
    df = pd.concat([corpus_exploded, categories], axis=1)
    pivoted_df = pd.pivot_table(df, index=['name', 'address'],
                                columns='category_name',
                                values='weight',
                                aggfunc='mean').reset_index().fillna(0)

    clean_data = pivoted_df.groupby(
      ['name', 'address']).mean().reset_index()
    return clean_data

  def _merge_data(self):
    corpus = self._clean_corpus_data()
    corpus_customer = pd.merge(left=self.customers, right=corpus,
                               left_on='name',
                               right_on='name')
    return corpus_customer

  def run(self):
    corpus_customer = self._merge_data()
    corpus_customer.to_csv(self.config.output_file, index=False)
