import argparse
from pathlib import Path

from app.utils.exceptions import MissingConfigArgsErrorMeta

"""
set configuration arguments as class attributes
"""


class Config(object):
  def __init__(self, **kwargs):
    for k, v in kwargs.items():
      setattr(self, k, v)


"""
get configuration arguments
"""


def get_config(**kwargs):
  parser = argparse.ArgumentParser()

  parser.add_argument('--mode', type=str, default='process')

  # app args
  parser.add_argument('--customers_input', type=Path)
  parser.add_argument('--corpus_input', type=Path)
  parser.add_argument('--output_file', type=Path)
  args = parser.parse_args()
  if (
          args.customers_input == None or args.corpus_input == None or
          args.output_file == None and args.length == None):
    parser.print_help()
    raise MissingConfigArgsErrorMeta
  args = vars(args)
  args.update(kwargs)

  return Config(**args)
