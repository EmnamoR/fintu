from app.analyser import Analyser
from app.configs.config import get_config
from app.utils.logger import get_logger

logger = get_logger(__name__)

if __name__ == '__main__':
  config = get_config()
  analyser = Analyser(config)
  analyser.run()
